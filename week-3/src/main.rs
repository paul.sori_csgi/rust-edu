use serde::{Deserialize, Serialize};
use sqlx::{
  sqlite::{SqlitePool, SqliteConnectOptions, SqliteJournalMode, SqlitePoolOptions, SqliteSynchronous},
};
use std::time::{Duration};
use std::{ str::FromStr };
use tide::{Body, Request, Response, StatusCode};
use toml;
use std::fs::File;
use std::fs;
use std::path::Path;
use std::process::exit;

#[derive(Deserialize, Clone, Debug)]
#[allow(non_snake_case)]
pub struct Config {
  #[serde(default="default_port")]
  pub port: u16,
  #[serde(default="default_db_file")]
  pub dbFile: String
}

fn default_port() -> u16 {
  1111
}

fn default_db_file() -> String {
  "meme-machine.sqlite".to_string()
}

pub trait Load {
  fn init () -> Self;
}

impl Load for Config {
  fn init () -> Config {
    let filename = "./meme-conf.toml";
    if !Path::new(filename).exists() {
      match File::create(filename) {
        Ok(_) => (),
        Err(_) => {
          // Write `msg` to `stderr`.
          eprintln!("Could not create file `{}`", filename);
          // Exit the program with exit code `1`.
          exit(1);
        }
      }
    }

    let contents = match fs::read_to_string(filename) {
      // If successful return the files text as `contents`.
      // `c` is a local variable.
      Ok(c) => c,
      // Handle the `error` case.
      Err(_) => {
          // Write `msg` to `stderr`.
          eprintln!("Could not read file `{}`", filename);
          // Exit the program with exit code `1`.
          exit(1);
      }
    };

    // load config file 
    let config: Config = toml::from_str(&contents).unwrap();

    config
  }
}

#[derive(Clone)]
pub struct State {
  db: SqlitePool,
  conf: Config
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Meme {
  pub uuid: String,
  pub filepath: String,
  pub metadata: Option<String>
}

#[async_std::main]
async fn main() -> Result<(),Box<dyn std::error::Error>> {
  let conf = Config::init();
  let state = State { db: setup_db(&conf.dbFile).await?, conf };
  let port = state.conf.port.clone();

  sqlx::migrate!("./migrations").run(&state.db).await?;

  tide::log::start();

  let mut app = tide::with_state(state);
  app.with(tide::log::LogMiddleware::new());

  app.at("/").get(|_| async { Ok("Hello, world!") });

  app.at("/test").get(|req: Request<State>| async move {
    let pool = &req.state().db;
    let rows = sqlx::query!(r#"SELECT * FROM memes;"#).fetch_all(pool).await?;
    let mut s = String::from("[");
    let mut n = 0;

    for rec in rows {
      s.push_str("{\"id\":\"");
      s.push_str(&rec.uuid);
      s.push_str("\",\"fp\":\"");
      s.push_str(&rec.filepath);
      s.push_str("\",\"md\":\"");
      s.push_str( &rec.metadata.as_deref().unwrap_or("d"));
      s.push_str("\"},");

      n = n + 1;
    }
    if n > 0 {
      s.pop();
    }
    s.push_str("]");

    Ok(s)
  });

  app.at("/test-2").get(|req: Request<State>| async move {
    let pool = &req.state().db;

    let rows: Vec<Meme> =
      sqlx::query_as!(Meme, r#"select uuid, filepath, metadata from memes"#).fetch_all(pool).await?;

    let mut res = Response::new(StatusCode::Ok);
    res.set_body(Body::from_json(&rows)?);
    Ok(res)
  });

  app.listen(format!("127.0.0.1:{}", &port.to_string())).await?;
  Ok(())
}

async fn setup_db(db_file: &String) -> Result<SqlitePool, Box<dyn std::error::Error>> {
  let database_url = format!("sqlite://{}", db_file);

  let pool_timeout = Duration::from_secs(30);
  let connection_options = SqliteConnectOptions::from_str(&database_url)?
    .create_if_missing(true)
    .journal_mode(SqliteJournalMode::Wal)
    .synchronous(SqliteSynchronous::Normal)
    .busy_timeout(pool_timeout);

  let pool = SqlitePoolOptions::new()
    .connect_with(connection_options)
    .await?;

  Ok(pool)
}