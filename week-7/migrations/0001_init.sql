PRAGMA foreign_keys = ON;

CREATE TABLE IF NOT EXISTS memes (
  uuid TEXT NOT NULL PRIMARY KEY,
  filepath TEXT UNIQUE NOT NULL,
  metadata TEXT,
  scan_id TEXT NOT NULL
);
