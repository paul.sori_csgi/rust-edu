PRAGMA foreign_keys = ON;

CREATE TABLE IF NOT EXISTS memes (
  uuid TEXT NOT NULL PRIMARY KEY,
  filepath TEXT NOT NULL,
  metadata TEXT
);
