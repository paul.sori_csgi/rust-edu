use serde::{Deserialize, Serialize};
use sqlx::{
  sqlite::{SqlitePool, SqliteConnectOptions, SqliteJournalMode, SqlitePoolOptions, SqliteSynchronous},
};
use std::time::{Duration};
use std::{ str::FromStr };

use crate::conf::Config;
use crate::conf::Load;
mod conf;

mod api;

#[derive(Clone)]
pub struct State {
  db: SqlitePool,
  conf: Config
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Meme {
  pub uuid: String,
  pub filepath: String,
  pub metadata: Option<String>
}

#[async_std::main]
async fn main() -> Result<(),Box<dyn std::error::Error>> {
  let conf = Config::init();
  let state = State { db: setup_db(&conf.dbFile).await?, conf };
  let port = state.conf.port.clone();

  sqlx::migrate!("./migrations").run(&state.db).await?;

  tide::log::start();

  let mut app = tide::with_state(state);
  app.with(tide::log::LogMiddleware::new());

  api::public::public_api(&mut app);

  app.listen(format!("127.0.0.1:{}", &port.to_string())).await?;
  Ok(())
}

async fn setup_db(db_file: &String) -> Result<SqlitePool, Box<dyn std::error::Error>> {
  let database_url = format!("sqlite://{}", db_file);

  let pool_timeout = Duration::from_secs(30);
  let connection_options = SqliteConnectOptions::from_str(&database_url)?
    .create_if_missing(true)
    .journal_mode(SqliteJournalMode::Wal)
    .synchronous(SqliteSynchronous::Normal)
    .busy_timeout(pool_timeout);

  let pool = SqlitePoolOptions::new()
    .connect_with(connection_options)
    .await?;

  Ok(pool)
}