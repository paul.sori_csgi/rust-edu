use tide::Server;
use tide::{Body, Request, Response, StatusCode};


use crate::State;
use crate::Meme;


pub fn public_api(app: &mut Server<State>) {
  app.at("/test-2").get(|req: Request<State>| async move {
    let pool = &req.state().db;

    let rows: Vec<Meme> =
      sqlx::query_as!(Meme, r#"select uuid, filepath, metadata from memes"#).fetch_all(pool).await?;

    let mut res = Response::new(StatusCode::Ok);
    res.set_body(Body::from_json(&rows)?);
    Ok(res)
  });
}