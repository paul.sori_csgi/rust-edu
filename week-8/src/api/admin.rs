use std::thread;
use futures::executor;
use tide::{Request, Server};
use std::time::Duration;
use sqlx::{ sqlite::{SqlitePool} };
use std::fs;
use uuid::Uuid;
use std::path::Path;

use crate::State;

pub fn admin_api(app: &mut Server<State>) {
  app.at("/admin/scan").get(|req: Request<State>| async move {    
    scan_on_new_thread(req.state().conf.memeFolder.clone().to_string(), req.state().db.clone());

    Ok("{}")
  });
}

fn scan_on_new_thread(folder: String, sql: SqlitePool) {
  thread::spawn(move || {
    let scan_id = Uuid::new_v4().to_string();
    recursive_scan(&folder, &sql, &scan_id);

    // Delete files that no longer exist
    let delete_sql = "DELETE FROM memes WHERE scan_id NOT IN (?)";
    executor::block_on(sqlx::query(delete_sql)
      .bind(&scan_id)
      .execute(&sql));

    thread::sleep(Duration::from_millis(1000));
  });
}

fn recursive_scan(folder: &String, sql: &SqlitePool, scan_id: &String) {
  let paths = fs::read_dir(&folder).unwrap();
  for path in paths {
    let foo = path.unwrap();

    let p = &foo.file_name().to_string_lossy().into_owned();

    if foo.path().is_dir() == true {
      let joined: String = Path::new(&folder).join(&p).into_os_string().into_string().unwrap();
      recursive_scan(&joined, sql, scan_id);
      continue;
    }

    let x = "INSERT INTO memes(filepath,uuid,scan_id) VALUES(?,?,?)
    ON CONFLICT(filepath) DO UPDATE SET scan_id= ?;";
    executor::block_on(sqlx::query(x)
      .bind(p)
      .bind(Uuid::new_v4().to_string())
      .bind(scan_id)
      .bind(scan_id)
      .execute(sql));
  }
}