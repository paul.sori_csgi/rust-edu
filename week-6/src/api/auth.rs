use tide::Server;
use tide::{Body, Request, Response, StatusCode};
use serde::{Deserialize, Serialize};
use serde_json::Value;

use crate::State;

#[derive(Debug, Deserialize, Serialize)]
#[allow(non_snake_case)]
struct LoginBody {
  username: String,
  password: String
}

#[derive(Debug, Deserialize, Serialize)]
#[allow(non_snake_case)]
struct TestJson {
  req: bool,
  opt: Option<String>,
  any: Value,
  #[serde(default="test_default")]
  def: String,
  enu: String,
  rec: Option<Vec<RecursiveJson>>,
  recx: Option<Box<RecursiveJson>>
}

#[derive(Debug, Deserialize, Serialize)]
struct RecursiveJson {
  foo: String,
  rec: Option<Vec<RecursiveJson>>,
  recx: Option<Box<RecursiveJson>>
}

fn test_default() -> String {
  "test".to_string()
}

pub fn auth_api(app: &mut Server<State>) {
  app.at("/login").post(|mut req: Request<State>| async move {
    let data: LoginBody = req.body_json().await?;

    let mut res = Response::new(StatusCode::Ok);
    res.set_body(Body::from_json(&data)?);
    Ok(res)
  });

  app.at("/test-json").post(|mut req: Request<State>| async move {
    let data: TestJson = req.body_json().await?;

    let mut res = Response::new(StatusCode::Ok);
    res.set_body(Body::from_json(&data)?);
    Ok(res)
  });
}