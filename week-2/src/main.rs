use serde::{Deserialize, Serialize};
use sqlx::{
  sqlite::{SqlitePool, SqliteConnectOptions, SqliteJournalMode, SqlitePoolOptions, SqliteSynchronous},
};
use std::time::{Duration};
use std::{ str::FromStr };
use tide::{Body, Request, Response, StatusCode};
use std::env;

#[derive(Clone)]
pub struct State {
  db: SqlitePool
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Meme {
  pub uuid: String,
  pub filepath: String,
  pub metadata: Option<String>
}

#[async_std::main]
async fn main() -> Result<(),Box<dyn std::error::Error>> {
  let state = State { db: setup_db().await? };

  sqlx::migrate!("./migrations").run(&state.db).await?;

  tide::log::start();

  let mut app = tide::with_state(state);
  app.with(tide::log::LogMiddleware::new());

  app.at("/").get(|_| async { Ok("Hello, world!") });

  app.at("/test").get(|req: Request<State>| async move {
    let pool = &req.state().db;
    let rows = sqlx::query!(r#"SELECT * FROM memes;"#).fetch_all(pool).await?;
    let mut s = String::from("[");
    let mut n = 0;

    for rec in rows {
      s.push_str("{\"id\":\"");
      s.push_str(&rec.uuid);
      s.push_str("\",\"fp\":\"");
      s.push_str(&rec.filepath);
      s.push_str("\",\"md\":\"");
      s.push_str( &rec.metadata.as_deref().unwrap_or("d"));
      s.push_str("\"},");

      n = n + 1;
    }
    if n > 0 {
      s.pop();
    }
    s.push_str("]");

    Ok(s)
  });

  app.at("/test-2").get(|req: Request<State>| async move {
    let pool = &req.state().db;

    let rows: Vec<Meme> =
      sqlx::query_as!(Meme, r#"select uuid, filepath, metadata from memes"#).fetch_all(pool).await?;

    let mut res = Response::new(StatusCode::Ok);
    res.set_body(Body::from_json(&rows)?);
    Ok(res)
  });

  app.listen("127.0.0.1:8080").await?;
  Ok(())
}

async fn setup_db() -> Result<SqlitePool, Box<dyn std::error::Error>> {
  let database_url = "sqlite://meme-machine.sqlite";

  let pool_timeout = Duration::from_secs(30);
  let connection_options = SqliteConnectOptions::from_str(&database_url)?
    .create_if_missing(true)
    .journal_mode(SqliteJournalMode::Wal)
    .synchronous(SqliteSynchronous::Normal)
    .busy_timeout(pool_timeout);

  let pool = SqlitePoolOptions::new()
    .connect_with(connection_options)
    .await?;

  Ok(pool)
}